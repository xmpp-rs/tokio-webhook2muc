use std::str::FromStr;
use std::convert::TryFrom;
use futures::{Future, Sink, Stream, sync::mpsc};
use tokio_xmpp::{Client, Packet, Event};
use xmpp_parsers::{Jid, BareJid, FullJid, Element};
use xmpp_parsers::message::{Body, Message, MessageType};
use xmpp_parsers::presence::{Presence, Show as PresenceShow, Type as PresenceType};
use xmpp_parsers::muc::Muc;

pub struct Agent {
    sender_tx: mpsc::UnboundedSender<Packet>,
    room_jid: BareJid,
}

impl Agent {
    pub fn new(jid: &str, password: &str, participant_jid: &str) -> (Box<dyn Future<Item = (), Error = ()>>, Self) {
        let participant_jid: FullJid = match FullJid::from_str(participant_jid) {
            Ok(jid) => jid,
            Err(err) => panic!("MUC Jid invalid: {:?}", err),
        };
        let (sender_tx, sender_rx) = mpsc::unbounded();

        let client = Client::new(jid, password).unwrap();
        let (sink, stream) = client.split();

        let reader = {
            let sender_tx = sender_tx.clone();
            let participant_jid = participant_jid.clone();
            let muc_jid: BareJid = participant_jid.clone().into();
            let jid = BareJid::from_str(jid).unwrap();
            stream.for_each(move |event| {
                match event {
                    Event::Online => {
                        info!("XMPP client now online at {}", jid);
                        let packet = Packet::Stanza(make_join_presence(participant_jid.clone()));
                        sender_tx.unbounded_send(packet)
                            .unwrap();
                    }
                    Event::Stanza(el) => {
                        if el.is("presence", "jabber:client") {
                            match Presence::try_from(el) {
                                Ok(presence) => {
                                    if presence.from == Some(Jid::Full(participant_jid.clone())) {
                                        if presence.type_ == PresenceType::Error {
                                            info!("Failed to enter MUC {:?}", muc_jid);
                                        } else {
                                            info!("Entered MUC {:?}", muc_jid);
                                        }
                                    }
                                },
                                Err(err) => debug!("Received invalid presence: {:?}", err),
                            }
                        }
                    }
                    _ => {}
                }

                Ok(())
            })
        };

        let sender = sender_rx
            .map_err(|e| panic!("Sink error: {:?}", e))
            .forward(sink)
            .map(|(rx, mut sink)| {
                drop(rx);
                let _ = sink.close();
            });

        let future = reader.select(sender)
            .map(|_| ())
            .map_err(|_| ());

        let agent = Agent {
            room_jid: participant_jid.clone().into(),
            sender_tx,
        };

        (Box::new(future), agent)
    }

    pub fn send_room_text(&mut self, text: String) {
        let mut message = Message::new(Some(Jid::Bare(self.room_jid.clone())));
        message.type_ = MessageType::Groupchat;
        message.bodies.insert("".to_string(), Body(text));
        self.sender_tx.unbounded_send(Packet::Stanza(message.into()))
            .unwrap();
    }
}

fn make_join_presence(participant_jid: FullJid) -> Element {
    let mut presence = Presence::new(PresenceType::None)
        .with_to(Some(Jid::Full(participant_jid)))
        .with_show(PresenceShow::Chat);
    presence.set_status("en".to_string(), "Your friendly Webhook spammer".to_string());
    presence.add_payload(Muc::new());
    presence.into()
}
